package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    public abstract Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    Map<String, Object> getGoodsListByPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void saveOrUpdate(Goods goods, String goodsId);

    void deleteById(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveOrUpdateStock(Integer inventoryQuantity, double purchasingPrice, Integer goodsId);

    void deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();
}
