package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;

public interface OverflowListService {
    void saveOverflowList(OverflowList overflowList, String overflowListGoodsStr);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
