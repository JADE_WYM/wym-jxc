package com.atguigu.jxc.service;

import java.util.Map;

public interface UnitService {
    public abstract Map<String, Object> getUnit();
}
