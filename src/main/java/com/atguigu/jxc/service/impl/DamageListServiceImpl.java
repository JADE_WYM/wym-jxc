package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageGoodsDao;
import com.atguigu.jxc.dao.DamageGoodsListDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListServiceImpl implements DamageListService {

    @Autowired
    private DamageGoodsDao damageGoodsDao;

    @Autowired
    private DamageGoodsListDao damageGoodsListDao;

    @Override
    public void saveDamageList(DamageList damageList, String damageListGoodsStr, String damageNumber) {
        // 先对t_damage_list表进行保存
        damageGoodsDao.saveDamageList(damageList);

        // 对t_damage_list_goods表进行保存
        List<DamageListGoods> damageListGoodsList = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);
        damageListGoodsList.forEach(damageListGoods -> {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageGoodsListDao.saveDamageGoodsList(damageListGoods);
        });
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageListList = damageGoodsDao.list(sTime,eTime);
        map.put("rows", damageListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageGoodsListDao.goodsList(damageListId);
        map.put("rows", damageListGoodsList);
        return map;
    }
}
