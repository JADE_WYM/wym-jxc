package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {

        Map<String,Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Supplier> supplierList = supplierDao.getSupplierListByPage(offSet, rows,supplierName);

        map.put("total",supplierList.size());
        map.put("rows",supplierList);

        return map;
    }

    @Override
    public void saveOrUpdate(Supplier supplier, String supplierId) {
        if (StringUtil.isEmpty(supplierId)){
            supplierDao.saveSupplier(supplier);
        }else {
            supplierDao.updateSupplier(supplier,supplierId);
        }
    }

    @Override
    public void delete(String ids) {
        supplierDao.delete(ids);
    }

}
