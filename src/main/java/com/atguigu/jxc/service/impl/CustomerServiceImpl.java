package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> getCustomer(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Customer> customerList = customerDao.getCustomerListByPage(offSet,rows,customerName);

        map.put("total",customerList.size());
        map.put("rows",customerList);

        return map;
    }

    @Override
    public void saveOrUpdate(Customer customer, String customerId) {
        if (StringUtil.isEmpty(customerId)){
            customerDao.saveCustomer(customer);
        }else {
            customerDao.updateCustomer(customer,customerId);
        }
    }

    @Override
    public void delete(String ids) {
        customerDao.delete(ids);
    }
}
