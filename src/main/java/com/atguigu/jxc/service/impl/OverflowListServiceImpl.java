package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListServiceImpl implements OverflowListService {

    @Autowired
    private OverflowListDao overflowListDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Override
    public void saveOverflowList(OverflowList overflowList, String overflowListGoodsStr) {
        // 先对t_overflow_list表进行保存
        overflowListDao.saveOverflowList(overflowList);

        // 对t_overflow_list_goods表进行保存
        List<OverflowListGoods> overflowListGoodsList = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);
        overflowListGoodsList.forEach(overflowListGoods -> {
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);
        });
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowListList = overflowListDao.list(sTime,eTime);
        map.put("rows", overflowListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.goodsList(overflowListId);
        map.put("rows", overflowListGoodsList);
        return map;
    }
}
