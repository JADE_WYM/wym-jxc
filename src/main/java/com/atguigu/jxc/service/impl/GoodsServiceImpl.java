package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.*;
import com.atguigu.jxc.util.StringUtil;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private GoodsTypeServiceImpl goodsTypeServiceImpl;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getGoodsListByPage(offSet, rows, codeOrName, goodsTypeId);

        map.put("total", goodsList.size());
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public Map<String, Object> getGoodsListByPage(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        ArrayList<Object> allGoodsType = goodsTypeServiceImpl.getAllGoodsType(goodsTypeId);
        ArrayList<Integer> allGoodsTypeIdList = new ArrayList<>();
        allGoodsType.forEach(obj -> {
            Integer childGoodsTypeId = (Integer) ((Map<String, Object>) obj).get("id");
            allGoodsTypeIdList.add(childGoodsTypeId);
        });
        // 补充代码，当根据id查询完子节点的id时，在ids中补充自己的id
        if (goodsTypeId != null) {
            allGoodsTypeIdList.add(goodsTypeId);
        }
        String allGoodsTypeIds = allGoodsTypeIdList.toString().replace("[", "").replace("]", "");

        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getGoodsListByPageWithIds(offSet, rows, goodsName, allGoodsTypeIds);

        map.put("total", goodsList.size());
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public void saveOrUpdate(Goods goods, String goodsId) {
        if (StringUtil.isEmpty(goodsId)) {
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.saveGoods(goods);
        } else {
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.updateGoods(goods, goodsId);
        }
    }

    @Override
    public void deleteById(Integer goodsId) {
        goodsDao.delete(goodsId);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);

        map.put("total", goodsList.size());
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);

        map.put("total", goodsList.size());
        map.put("rows", goodsList);

        return map;
    }

    @Override
    public void saveOrUpdateStock(Integer inventoryQuantity, double purchasingPrice, Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.updateGoods(goods, goodsId.toString());
    }

    @Override
    public void deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getGoodsById(goodsId);
        if (goods.getState() == 0) {
            goods.setInventoryQuantity(0);
            goodsDao.updateGoods(goods, goodsId.toString());
        }
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goodsList = goodsDao.listAlarm();

        map.put("rows", goodsList);
        return map;
    }


}
