package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import java.util.Map;

public interface DamageListService {
    void saveDamageList(DamageList damageList, String damageListGoodsStr, String damageNumber);

    Map<String, Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer damageListId);
}
