package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    Map<String, Object> getCustomer(Integer page, Integer rows, String customerName);

    void saveOrUpdate(Customer customer, String customerId);

    void delete(String ids);
}
