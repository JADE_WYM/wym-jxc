package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    public abstract Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName);

    public abstract void saveOrUpdate(Supplier supplier, String supplierId);

    public abstract void delete(String ids);
}
