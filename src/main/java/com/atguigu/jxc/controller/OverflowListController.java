package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class OverflowListController {

    @Autowired
    private OverflowListService overflowListService;

    @PostMapping("/overflowListGoods/save")
    @ResponseBody
    public ServiceVO saveOverflowList(OverflowList overflowList, String overflowListGoodsStr){
        overflowListService.saveOverflowList(overflowList,overflowListGoodsStr);
        return new ServiceVO<>(100,"报损商品保存成功");
    }

    @PostMapping("/overflowListGoods/list")
    @ResponseBody
    public Map<String,Object> list(String sTime, String eTime){
        Map<String,Object> map = overflowListService.list(sTime,eTime);
        return map;
    }

    @PostMapping("/overflowListGoods/goodsList")
    @ResponseBody
    public Map<String,Object> goodsList(Integer overflowListId){
        Map<String,Object> map = overflowListService.goodsList(overflowListId);
        return map;
    }
}
