package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@Controller
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping(value = "goods/listInventory")
    @ResponseBody
    public Map<String,Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId){

        Map<String,Object> map = goodsService.listInventory(page, rows, codeOrName, goodsTypeId);
        return map;

    }


    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping(value = "goods/list")
    @ResponseBody
    public Map<String,Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId){

        Map<String,Object> map = goodsService.getGoodsListByPage(page, rows, goodsName, goodsTypeId);
        return map;

    }


    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/goods/getCode")
    @RequiresPermissions(value = "商品管理")
    @ResponseBody
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/goods/save")
    @ResponseBody
    public ServiceVO saveOrUpdate(Goods goods, @RequestParam(value = "goodsId",required = false) String goodsId) {
        goodsService.saveOrUpdate(goods, goodsId);
        return new ServiceVO<>(100, "操作成功！");
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/delete")
    @ResponseBody
    public ServiceVO deleteById(Integer goodsId){
        goodsService.deleteById(goodsId);
        return new ServiceVO<>(100, "操作成功！");
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getNoInventoryQuantity")
    @ResponseBody
    public Map<String,Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        Map<String,Object> map = goodsService.getNoInventoryQuantity(page, rows,nameOrCode);
        return map;
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getHasInventoryQuantity")
    @ResponseBody
    public Map<String,Object> getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        Map<String,Object> map = goodsService.getHasInventoryQuantity(page, rows,nameOrCode);
        return map;
    }


    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @PostMapping("/goods/saveStock")
    @ResponseBody
    public ServiceVO saveOrUpdateStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice) {
        goodsService.saveOrUpdateStock(inventoryQuantity,purchasingPrice, goodsId);
        return new ServiceVO<>(100, "操作成功！");
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/deleteStock")
    @ResponseBody
    public ServiceVO deleteStock(Integer goodsId){
        goodsService.deleteStock(goodsId);
        return new ServiceVO<>(100, "操作成功！");
    }

    /**
     * 查询库存报警商品信息
     * @return
     */
    @PostMapping("/goods/listAlarm")
    @ResponseBody
    public Map<String,Object> listAlarm(){
        Map<String,Object> map = goodsService.listAlarm();
        return map;
    }
}
