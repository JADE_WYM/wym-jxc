package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class DamageListController {

    @Autowired
    private DamageListService damageListService;

    @PostMapping("/damageListGoods/save")
    @ResponseBody
    public ServiceVO savedamageListGoods(DamageList damageList, String damageListGoodsStr, @RequestParam("damageNumber") String damageNumber){

        damageListService.saveDamageList(damageList,damageListGoodsStr,damageNumber);

        return new ServiceVO<>(100,"报损商品保存成功");
    }

    @PostMapping("/damageListGoods/list")
    @ResponseBody
    public Map<String,Object> list(String sTime, String eTime){
        Map<String,Object> map = damageListService.list(sTime,eTime);
        return map;
    }

    @PostMapping("/damageListGoods/goodsList")
    @ResponseBody
    public Map<String,Object> goodsList(Integer damageListId){
        Map<String,Object> map = damageListService.goodsList(damageListId);
        return map;
    }
}
