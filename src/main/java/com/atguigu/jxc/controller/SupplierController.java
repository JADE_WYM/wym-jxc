package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @PostMapping("/list")
    @ResponseBody
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
        Map<String, Object> resultMap = supplierService.getSupplierList(page, rows, supplierName);
        return resultMap;
    }

    @PostMapping("/save")
    @ResponseBody
    public ServiceVO saveOrUpdate(Supplier supplier, @RequestParam(value = "supplierId",required = false) String supplierId) {
        supplierService.saveOrUpdate(supplier, supplierId);
        return new ServiceVO<>(100, "操作成功！");
    }

    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO deleteByIds(String ids) {
        supplierService.delete(ids);
        return new ServiceVO<>(100, "删除操作成功！");
    }
}
