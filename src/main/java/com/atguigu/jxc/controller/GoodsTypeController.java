package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * @description 商品类别控制器
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询所有商品类别
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/loadGoodsType")
    @RequiresPermissions(value={"商品管理","进货入库","退货出库","销售出库","客户退货","当前库存查询","商品报损","商品报溢","商品采购统计"},logical = Logical.OR)
    public ArrayList<Object> loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }

    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(String goodsTypeName,Integer pId) {
        goodsTypeService.save(goodsTypeName,pId);
        return new ServiceVO<>(100, "操作成功！");
    }

    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(Integer  goodsTypeId) {
        goodsTypeService.delete(goodsTypeId);
        return new ServiceVO<>(100, "操作成功！");
    }

}
