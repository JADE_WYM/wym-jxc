package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> getCustomer(Integer page,Integer rows,String customerName){

        Map<String,Object> map = customerService.getCustomer(page,rows,customerName);
        return map;
    }

    @PostMapping("/save")
    @ResponseBody
    public ServiceVO saveOrUpdate(Customer customer, @RequestParam(value = "customerId",required = false) String customerId) {
        customerService.saveOrUpdate(customer, customerId);
        return new ServiceVO<>(100, "操作成功！");
    }

    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO deleteByIds(String ids) {
        customerService.delete(ids);
        return new ServiceVO<>(100, "删除操作成功！");
    }
}
