package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SupplierDao {
    List<Supplier> getSupplierListByPage(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    void saveSupplier(@Param("supplier") Supplier supplier);

    void updateSupplier(@Param("supplier") Supplier supplier, @Param("supplierId") String supplierId);

    void delete(@Param("ids") String ids);
}
