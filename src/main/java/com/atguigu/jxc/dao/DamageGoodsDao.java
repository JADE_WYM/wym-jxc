package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DamageGoodsDao {
    void saveDamageList(DamageList damageList);

    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime")String eTime);

}
