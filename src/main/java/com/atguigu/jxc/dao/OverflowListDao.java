package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OverflowListDao {
    void saveOverflowList(OverflowList overflowList);

    List<OverflowList> list(@Param("sTime")String sTime, @Param("eTime")String eTime);
}
