package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
@Mapper
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void save(@Param("goodsTypeName") String goodsTypeName, @Param("pId")Integer pId);

    void delete(@Param("goodsTypeId") Integer goodsTypeId);

    GoodsType getGoodsTypeById(Integer pId);
}
