package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
@Mapper
public interface GoodsDao {

    String getMaxCode();

    List<Goods> getGoodsListByPage(@Param("offSet")int offSet, @Param("rows")Integer rows, @Param("codeOrName")String codeOrName, @Param("goodsTypeId")Integer goodsTypeId);

    List<Goods> getGoodsListByPageWithIds(@Param("offSet")int offSet, @Param("rows")Integer rows, @Param("codeOrName")String codeOrName, @Param("allGoodsTypeIdList")String allGoodsTypeIdList);

    void saveGoods(@Param("goods") Goods goods);

    void updateGoods(@Param("goods")Goods goods, @Param("goodsId")String goodsId);

    void delete(@Param("goodsId") Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("offSet")int offSet, @Param("rows")Integer rows, @Param("nameOrCode")String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("offSet")int offSet, @Param("rows")Integer rows, @Param("nameOrCode")String nameOrCode);

    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    List<Goods> listAlarm();
}
