package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerDao {
    List<Customer> getCustomerListByPage(@Param("offSet")int offSet, @Param("rows")Integer rows, @Param("customerName")String customerName);

    void saveCustomer(@Param("customer")Customer customer);

    void updateCustomer(@Param("customer")Customer customer, @Param("customerId")String customerId);

    void delete(@Param("ids")String ids);
}
